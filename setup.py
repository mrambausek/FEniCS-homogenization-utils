from setuptools import setup

setup(
    name='Computational Homogenization Utils for FEniCS',
    version='0.0.1',
    packages=['fenics_homogenization_utils'],
    url='',
    license='GPL v3',
    author='Matthias Rambausek',
    author_email='matthias.rambausek@mechbau.uni-stuttgart.de',
    description='A lib providing some useful tools for computational homogenization with FEniCS.'
)
