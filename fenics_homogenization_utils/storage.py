from dolfin.cpp.la import Vector
from dolfin.functions.function import Function, FunctionSpace
from dolfin.cpp.mesh import Mesh
from dolfin.cpp.la import Vector
from dolfin.cpp.io import HDF5File
import numpy as np

from pdecoeffserver.pypdecoeffserver import np_to_dict, np_from_dict


def key_from_coords(coords: np.ndarray, cell: int) -> str:
    if cell is None:
        return '|'.join(['{:1.16e}'.format(c) for c in coords])
    else:
        return '|'.join(['{:d}'.format(cell)] + ['{:1.16e}'.format(c) for c in coords])


def cell_and_coords_from_key(key: str) -> (int, np.ndarray):
    """
    :param key: string
    :return: coords:np.ndarray, cell:int
    """
    data = key.split('|')
    cell = np.int_(data[0])
    coords = np.array([np.float_(c) for c in data[1:]])
    return cell, coords


class HomogenizedData(object):
    def __init__(self, valid=False,
                 energy=None,
                 fluxes=None,
                 moduli=None):
        self.valid = valid

        def_array = np.empty(0, dtype=np.float_)

        if energy is None:
            energy = def_array.copy()
        self.energy = energy

        if fluxes is None:
            fluxes = def_array.copy()
        self.fluxes = fluxes

        if moduli is None:
            moduli = def_array.copy()
        self.moduli = moduli

    def data_dict(self):
        return {"energy": np_to_dict(self.energy),
                "fluxes": np_to_dict(self.fluxes),
                "moduli": np_to_dict(self.moduli)}

    def write_state(self, hdf5_file, h5group_name):
        if self.valid:
            hdf5_file.write(self.energy,
                            "/".join([h5group_name, "energy"]))
            hdf5_file.write(self.fluxes.flatten(),
                            "/".join([h5group_name, "fluxes", "values"]))
            hdf5_file.write(np.array(self.fluxes.shape, dtype=np.float_),
                            "/".join([h5group_name, "fluxes", "shape"]))
            hdf5_file.write(self.moduli.flatten(),
                            "/".join([h5group_name, "moduli", "values"]))
            hdf5_file.write(np.array(self.moduli.shape, dtype=np.float_),
                            "/".join([h5group_name, "moduli", "shape"]))

    def read_state(self, hdf5_file, h5group_name):
        self.valid = False
        try:
            v_energy = Vector()
            hdf5_file.read(v_energy, "/".join([h5group_name, "energy"]), False)
            self.energy = v_energy.array().copy()

            v_fluxes = (Vector(), Vector())
            hdf5_file.read(v_fluxes[0], "/".join([h5group_name, "fluxes", "values"]), False)
            hdf5_file.read(v_fluxes[1], "/".join([h5group_name, "fluxes", "shape"]), False)
            self.fluxes = v_fluxes[0].array().reshape(v_fluxes[1].array().astype(np.int)).copy()

            v_moduli = (Vector(), Vector())
            hdf5_file.read(v_moduli[0], "/".join([h5group_name, "moduli", "values"]), False)
            hdf5_file.read(v_moduli[1], "/".join([h5group_name, "moduli", "shape"]), False)
            self.moduli = v_moduli[0].array().reshape(v_moduli[1].array().astype(np.int)).copy()
            self.valid = True
        except Exception as e:
            raise e


class RVEData(object):
    def __init__(self, cell: int, coords: np.ndarray, macro_state: np.ndarray, micro_state: Function,
                 homogenized_data=None, atol=1e-12, rtol=1e-8):
        self.coords = coords
        self.cell = cell
        self._micro_state = micro_state.copy(deepcopy=True)
        self._macro_state = macro_state.copy()

        if homogenized_data is None:
            homogenized_data = HomogenizedData()
        self._data = homogenized_data
        self._atol = atol
        self._rtol = rtol

    def key(self) -> str:
        return key_from_coords(self.coords, self.cell)

    def update(self, macro_state: np.ndarray,
                     micro_state: Function,
                     homogenized_data: HomogenizedData) -> None:
        """
        :param macro_state: np.ndarray
        :param micro_state: Function
        :param homogenized_data: HomogenizedData
        
        :ret: None
        """
        self._micro_state.assign(micro_state)
        # FIXME: probably this needs a better test. E.g the mesh might be changed without changing number of dofs
        #try:
            #self._micro_state.assign(micro_state)
        #except:
            #self._micro_state = micro_state.copy(deepcopy=True)

        self._macro_state[...] = macro_state[...]
        self._data = homogenized_data

    def micro_state(self) -> Function:
        return self._micro_state

    def macro_state(self) -> np.ndarray:
        return self._macro_state

    def cmp_macro_state(self, macro_state: np.ndarray) -> bool:
        """
        :param macro_state: np.ndarray

        :ret: bool
        """
        return np.allclose(macro_state, self._macro_state, atol=self._atol, rtol=self._rtol)

    def data(self) -> dict:
        """
        :ret: dict
        """
        return self._data.data_dict()

    def valid(self) -> bool:
        return self._data.valid

    def info(self) -> dict:
        """
        :ret info:dict:
        """
        info = {'coords': self.coords,
                'cell': self.cell,
                'macro_state': np_to_dict(self._macro_state),
                'data': self.data(),
                'valid': self.valid()}
        return info

    def write_state(self, hdf5_file: HDF5File, h5group_name: str) -> None:
        """
        :param hdf5_file: HDF5File
        :param timestamp: str
        :ret: None
        """
        key = key_from_coords(self.coords, self.cell)
        hdf5_file.write(self._macro_state.flatten(),
                        "/".join([h5group_name, "macro_state", "values"]))
        hdf5_file.write(np.array(self._macro_state.shape, dtype=np.float_),
                        "/".join([h5group_name, "macro_state", "shape"]))
        hdf5_file.write(self._micro_state, "/".join([h5group_name, "micro_state"]))
        self._data.write_state(hdf5_file, h5group_name)

    def read_state(self, hdf5_file: HDF5File, h5group_name: str, element=None):
        """
        :param hdf5_file: HDF5File
        :param timestamp: str
        :ret: None
        """
        v_macro = (Vector(), Vector())
        hdf5_file.read(v_macro[0], "/".join([h5group_name, "macro_state", "values"]), False)
        hdf5_file.read(v_macro[1], "/".join([h5group_name, "macro_state", "shape"]), False)
        self._macro_state = v_macro[0].array().reshape(v_macro[1].array().astype(np.int))

        hdf5_file.read(self._micro_state, "/".join([h5group_name, "micro_state"]))

        self._data.read_state(hdf5_file, h5group_name)


class GlobalStorage(object):
    def __init__(self):
        self._rves = {}

    def rve_data(self, coords: np.ndarray, cell=None) -> RVEData:
        return self._rves[key_from_coords(coords, cell)]
    
    def rve_iterator(self):
        """
        :ret: A list resp. an iterator over the stored RVE_DATA objects
        """
        return self._rves.values()

    def add_rve_data(self, rve_data: RVEData) -> None:
        self._rves[rve_data.key()] = rve_data

    def has_rve(self, key) -> bool:
        return key in self._rves.keys()
