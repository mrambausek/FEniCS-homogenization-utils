from dolfin.cpp.mesh import SubDomain
from dolfin.cpp.function import near
import numpy as np

class Corner(SubDomain):
    def __init__(self, dim, edges, respect_on_boundary=False):
        super().__init__()
        self._edges = edges
        self._respect_on_boundary = respect_on_boundary
        self.dim = dim
        self.codim = 0
        
        if dim not in [2,3]:
            raise NotImplementedError("Corners are only implemented for 2d and 3d.")
    
    def inside(self, x, on_boundary):
        if self._respect_on_boundary:
            _on_boundary = on_boundary
        else:
            _on_boundary = True
            
        count = len(
            list(
                filter(lambda sd: sd.inside(x, _on_boundary), self._edges)
            )
        )
        return count == self.dim - self.codim



class Edge(SubDomain):
    def __init__(self, dim, faces, respect_on_boundary=False):
        super().__init__()
        self._faces = faces
        self._respect_on_boundary = respect_on_boundary
        self.dim = dim
        self.codim = 1
        
        if dim not in [2,3]:
            raise NotImplementedError("Edges are only implemented for 2d and 3d.")
    
    def inside(self, x, on_boundary):
        if self._respect_on_boundary:
            _on_boundary = on_boundary
        else:
            _on_boundary = True
            
        count = len(
            list(
                filter(lambda sd: sd.inside(x, _on_boundary), self._faces)
            )
        )
        return count == self.dim - self.codim

    
    
class PeriodicBC(SubDomain):
    """
    Periodic boundary both directions for 2D and 3D
    """
    def __init__(self, dim, included_sub_domains, excluded_sub_domains, mapped_sub_domains, outside=1.0e6):
        super().__init__()
        self._included = included_sub_domains
        self._excluded = excluded_sub_domains
        self._mapped = mapped_sub_domains
        self.dim = dim
        self._mappings = {}
        self._mappings[-1] = []
        for ii in np.linspace(1,2,2, dtype=np.int):
            self._mappings[ii] = []
        
        # check if mapped_sub_domains provide 'map'
        for sd in self._mapped:
            m = getattr(sd, 'map')
        
        if dim not in [2,3]:
            raise NotImplementedError("PeriodicBCs are only implemented for 2d and 3d.")
        
        try:
            if len(outside) != dim:
                raise Exception("If 'outside' is a list or an array, it has to be on length 'dim'.")
            else:
                self._outside = np.array(outside, dtype=np.float64)
        except TypeError as e:
            self._outside = [np.float64(outside)]*dim
    
    def inside(self, x, on_boundary):
        """
        This method should return True for points/positions x, if this location belong to a governing boundary.
         Otherwise it should return False.
        :param x: [in] points/position
        :param on_boundary: [in] if x belong to the boundary
        :return: [bool]
        """
        return any([sd.inside(x, on_boundary) for sd in self._included]) \
               and not any([sd.inside(x, on_boundary) for sd in self._excluded]) \
               and not any([sd.inside(x, on_boundary) for sd in self._mapped]) 
        
    def map(self, x, y):
        """
        This method maps "governed" points x to "governing" points y
        :param x: [in] point for which a governing points is to be determined, if x is a governed point.
        :param y: [out, write to] point, that governs x. If x is not governed, it might be useful to assign
        "nonsensical values" to the coordinates of y.
        :return: None
        """
        #FIXME: It is not allowed to map to a point that is not inside this domain.
        # Since any edge which belongs to a governed domain is exluded from this domain,
        # edges that do no belong to for such an intersection, currently fail to map correctly.
        # This can either be handled 
        # * via some trickery in the map() routine of the governed faces,
        # * by explicit specifications for edges,
        # * or by some clever stuff in edge that takes information from both of its faces.
        # Or one just codes something slightly less generic (e.g. one pbc class set for 
        # each number of faces).
        
        for ii in np.linspace(1, 2, 2, dtype=np.int):
            for msd in filter(lambda item: hasattr(item, "codim") and item.codim == ii and item.inside(x, True), 
                              self._mapped):            
                msd.map(x, y)
                self._mappings[ii].append([np.array(x), np.array(y)])
                return
            
        for msd in filter(lambda item: not hasattr(item, "codim") and item.inside(x, True), 
                          self._mapped):
            msd.map(x, y)
            self._mappings[-1].append([np.array(x), np.array(y)])
            return
        
        for ii in range(self.dim):
            y[ii] = self._outside[ii]
            
    
    def print_mappings(self):
        with open("pbc_mapping.log", "w") as mf:
            for kk in sorted(self._mappings.keys()):
                mf.write(str(kk) + "\n")
                for mm in self._mappings[kk]:
                    mf.write(str(mm) + "\n")
                mf.write("\n")
