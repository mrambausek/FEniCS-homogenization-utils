
import copy
import numpy as np
from dolfin.cpp.mesh import SubDomain
from dolfin.cpp.function import near

import itertools


def base_entities(entity_dim, index_set, target_set=None, add_set=None):
    add_set = [None] if add_set is None else add_set + [None]
    target_set = [] if target_set is None else target_set

    for ii in index_set:
        add_set[-1] = ii
        if len(add_set) == entity_dim:
            target_set.append(tuple(add_set))
        else:
            target_set = base_entities(entity_dim, filter(lambda jj: jj > ii, index_set), target_set,
                                       add_set=copy.deepcopy(add_set))
    return target_set


def shift_vectors(base_entity, index_set, target_set=None, add_set=None):
    add_set = [None] if add_set is None else add_set + [None]
    target_set = [] if target_set is None else target_set

    reduced_set = filter(lambda item: item not in base_entity, index_set)
    for ii in reduced_set:
        add_set[-1] = ii
        target_set.append(tuple(add_set))
        target_set = shift_vectors([], filter(lambda jj: jj > ii, reduced_set), target_set,
                                   add_set=copy.deepcopy(add_set))
    return target_set


def copy_components(_from, _to):
    for ii in range(len(_from)):
        _to[ii] = _from[ii]


class EntityData(object):

    def __init__(self, base_entity, shifts=None):
        self.base_entity = base_entity
        self.shifts = shifts
        self.entity_dim = len(base_entity)

    def __repr__(self):
        return "base entity: {:s}, shifts: {:s}".format(str(self.base_entity), str(self.shifts))

    def is_base_entity(self):
        return self.shifts is None

    def get_shift_vectors(self, basis):
        if self.shifts is None:
            return []
        return np.zeros_like(basis[:,0]) + [basis[:,sv] for sv in self.shifts]

    def shift_vector(self, basis):
        return sum(self.get_shift_vectors(basis))


class UnitCell(SubDomain):

    def __init__(self, basis_vectors, origin=None):
        
        super().__init__()

        basis = np.array([np.array(bv).flatten() for bv in basis_vectors]).T

        space_dim = basis.shape[0]
        dim = basis.shape[1]

        origin = -np.sum(basis, axis=1)/2 if origin is None else np.array(origin).flatten()

        if space_dim > 3:
            raise Exception("Unit Cell only supports spacedim up to 3.")

        if dim > space_dim:
            raise Exception("The number of basis vector cannot be greater than the spatial dimension.")

        if origin.size != space_dim:
            raise Exception("Spatial dimensions for origin and basis do not match.")

        for ii in range(dim):
            for jj in range(dim):
                if ii != jj and np.abs(np.dot(basis[:,ii], basis[:,jj])) > 1e-8:
                    raise Exception("Basis vector {:d} and {:d} are not orthogonal.".format(ii, jj))

        self._dim = dim
        self._space_dim = space_dim
        self._origin = origin
        self._basis = basis
        self._entities = {}
        self._init_entities()

    def _init_entities(self):
        for dd in range(self._dim):
            if dd == 0:
                _base_entities = [EntityData([])]
            else:
                _base_entities = [EntityData(ee) for ee in base_entities(dd, range(self._dim))]

            self._entities[dd] = copy.deepcopy(_base_entities)
            for ee in _base_entities:
                # print(ee, shift_vectors(ee.base_entity, range(self._dim)))
                self._entities[dd] += [EntityData(ee.base_entity, ss)
                                       for ss in shift_vectors(ee.base_entity, range(self._dim))]

    def l(self, ii):
        return self._basis[:, ii]

    def origin(self):
        return self._origin

    def entities(self, entity_dim=None):
        if entity_dim is None:
            return self._entities
        else:
            return self._entities[entity_dim]

    def inside_entity(self, v, entity):
        product_tests = \
            [np.abs(np.dot((v - entity.shift_vector(self._basis)), self.l(ii))) < 1e-5
             for ii in filter(lambda jj: jj not in entity.base_entity, range(self._dim))]
        
        if all(product_tests):
            return entity
        else:
            return None

    def inside_entities(self, v, filter_func=None, entity_dim=None):
        if filter_func is None:
            filter_func = lambda item: True

        if entity_dim is None:
            entities = []
            for dd in sorted(self._entities.keys()):
                entities += [ee for ee in filter(filter_func, self._entities[dd])]
        else:
            entities = [ee for ee in filter(filter_func, self._entities[entity_dim])]
        # print(len(entities))
        for entity in entities:
            if self.inside_entity(v, entity):
                return entity
        return None

    def space_dim(self):
        return self._space_dim

    def dim(self):
        return self._dim

    def entity_shift_vector(self, entity_data):
        if entity_data is None:
            return np.ones(self._dim)*1e6
        else:
            return entity_data.shift_vector(self._basis)


class UnitCellPeriodicBC(SubDomain):

    def __init__(self, unit_cell):
        super().__init__()
        self._unit_cell = unit_cell
        self._print_c = 0
        self._mapped = {0: [], 1: [], 2: []}

    def inside(self, x, on_boundary):
        if not on_boundary:
            return False

        v = x - self._unit_cell.origin()
        if self._unit_cell.inside_entities(v, filter_func=lambda ee: not ee.is_base_entity()) is not None:
            return False

        if self._unit_cell.inside_entities(v, filter_func=lambda ee: ee.is_base_entity()) is not None:
            return True

        return False

    def map(self, x, y):
        v = x - self._unit_cell.origin()
        entity = self._unit_cell.inside_entities(v, filter_func=lambda ee: not ee.is_base_entity()
                                                                           and ee.entity_dim < self._unit_cell.dim())
        copy_components(x - self._unit_cell.entity_shift_vector(entity), y)
        if entity is not None:
            self._mapped[entity.entity_dim].append([x.copy(), entity, self._unit_cell.entity_shift_vector(entity), y.copy()])


class UnitCellDirichletBC(SubDomain):
    def __init__(self, unit_cell):
        super().__init__()
        self._unit_cell = unit_cell
        self._origin = self._unit_cell.origin
        
    def inside(self, x, on_boundary):
        dim = self._unit_cell.dim()
        v = x - self._unit_cell.origin()
        
        _inside = []
        filter_func = lambda ee: ee not in _inside
        for ii in range(dim):
            ee = self._unit_cell.inside_entities(v, filter_func=filter_func, entity_dim=dim-1)
            if ee is None:
                return False
            _inside.append(ee)
        return True
            
