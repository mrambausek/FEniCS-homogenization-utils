import dolfin
import abc


class RVEProblem(abc.ABCMeta):
    """
    A class defining a compact interface for an RVE that can be use to determine material behaviour with
    homogenization techniques.
    """
    def __init__(self):
        pass

    @abc.abstractmethod
    def update_rve_properties(self, homogenized_quantities, identifier=None):
        """
        This method is supposed to update the homogenized quantities.
        :param homogenized_quantities: [in/out] A data structure providing the macro input as well as the homogenized
        quantities that are to be updated.
        :return: identifier, can be used for identifying somehow associated calls.
        """
        pass




