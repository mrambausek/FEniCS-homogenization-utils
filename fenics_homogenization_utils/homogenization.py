
import numpy as np
import dolfin
from dolfin import assemble, assemble_system, Constant, inner, dx, TestFunction
from dolfin.cpp.la import Matrix, Vector, LUSolver, lu_solver_methods, krylov_solver_methods
import time


class OperatorCache(object):
    def __init__(self):
        self.A_macro = Matrix()
        self.schur_complement_2 = Matrix()
        self.L = Matrix()
        self.A = Matrix()
        self.b = Vector()


def compute_homogenized_fluxes(G_int_macro, form_compiler_parameters=None):
    macro_fluxes = assemble(G_int_macro, form_compiler_parameters=form_compiler_parameters)
    vol = assemble(Constant(1.0) * dx(G_int_macro.ufl_domain()))
    return macro_fluxes.array()/vol


def compute_homogenized_moduli(G_int_micro_lin_micro,
                               G_int_micro_lin_macro,
                               G_int_macro_lin_macro,
                               dirichlet_bcs, linear_solver=None,
                               form_compiler_parameters=None,
                               operator_cache=None):
    # compute schur complement: A_macro - A_micro_macro^T * A_micro^{-1} * A_micro_macro
    # schur complement / V
    if operator_cache is None:
        operator_cache = OperatorCache()

    assemble(G_int_macro_lin_macro, tensor=operator_cache.A_macro, form_compiler_parameters=form_compiler_parameters)
    assemble(G_int_micro_lin_macro, tensor=operator_cache.L, form_compiler_parameters=form_compiler_parameters)

    _bcs = dirichlet_bcs
    if not hasattr(_bcs, "__len__"):
        _bcs = [_bcs]
    for _bc in _bcs:
        _bc.zero(operator_cache.L)
    operator_cache.L.apply('insert')

    try:
        V_micro = G_int_micro_lin_micro.function_spaces()[-1]
    except AttributeError:
        V_micro = G_int_micro_lin_micro.arguments()[-1].function_space()

    f_zero = Constant(np.zeros(V_micro.ufl_element().value_shape(), dtype=np.float_))
    b = inner(TestFunction(V_micro), f_zero) * dx
    # Problem, die Matrix A wird eigentlich schon vom nichtlinearen Löser verwendet.
    assemble_system(G_int_micro_lin_micro, b, dirichlet_bcs,
                    A_tensor=operator_cache.A, b_tensor=operator_cache.b,
                    form_compiler_parameters=form_compiler_parameters)

    if operator_cache.schur_complement_2.empty():
        operator_cache.schur_complement_2 = operator_cache.A_macro.copy()
    schur_complement_second_term(operator_cache.A,
                                 operator_cache.L,
                                 operator_cache.A_macro,
                                 operator_cache.schur_complement_2,
                                 linear_solver)

    vol = assemble(Constant(1.0) * dx(G_int_macro_lin_macro.ufl_domain()))
    return (operator_cache.A_macro.array() + operator_cache.schur_complement_2.array()) / vol


def extract_col(matrix, col_number, col_vector):
    # possibly simpler with PETSc4py, but this approach
    # is more general

    # prepare the vector for extracting the column via
    # a matrix vector multiplication:
    # col_vec = matrix . col_selector
    col_selector = Vector(dolfin.mpi_comm_world(), matrix.size(1))
    # one might argue that creating this vector
    # is expensive. however, this is neglible compared
    # with the overall effort. (col_selector is
    # rather short)

    # the vector entry corresponding to the column
    # col_number is set to one, the others
    # to zero
    vals = np.array([1.0], dtype=np.float64)
    rows = np.array([col_number], dtype=np.intc)
    col_selector.set_local(vals, rows)
    col_selector.apply('insert')

    # "write" column col_number to col_vector
    matrix.mult( col_selector, col_vector)


def schur_complement_second_term(A, L, C, second_term, linear_solver):
    # some vectors holding data
    A_inv_Li = Vector()
    L_col = Vector()
    _x = Vector()

    # seems to be slow!
    solver_class = None
    if linear_solver is None:
        linear_solver = LUSolver()
        linear_solver.parameters["reuse_factorization"] = True

    linear_solver.set_operator(A)

    # now let's compute the second term of the schur complement
    # for each column i in L compute:
    # softening_col = L^T. A^{inv} . L[:,i]
    # softening_term[:,i] = softening_col
    for i in range(L.size(1)):
        # extract the i-th column
        extract_col(L, i, L_col)

        # compute A^{inv}.L[:,i]
        linear_solver.solve(A_inv_Li, L_col)

        # compute L^T .(A^{inv}.L[:,i]) / V
        L.transpmult(A_inv_Li, _x)

        # write this result to column i of the softening term
        cols = np.array([i], dtype=np.intc)
        rows = np.arange(C.size(0), dtype=np.intc)
        second_term.set_local(-_x.array(), rows, cols)
    second_term.apply('insert')
